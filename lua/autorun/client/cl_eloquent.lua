--[[
    Create the gui
--]]

local function create()
    local width = 500
    local height = 400

    local gui = vgui.Create( "DFrame" )
    gui:SetVisible( false )
    gui:SetSize( width, height )
    gui:Center()
    gui:SetTitle( "Eloquent" )
    gui:ShowCloseButton( true )
    gui:MakePopup()

    return gui
end

--[[
    Manage the GUI
--]]

local gui
local visible = false

local function toggle()
    if not gui then
        gui = create()
    end

    gui:SetVisible( not gui:IsVisible() )
end

net.Receive( "Toggle Eloquent GUI", toggle )
