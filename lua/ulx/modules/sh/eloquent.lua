--[[
    Addon: ULX Eloquent
    Author: Timmy
    Contact: hello[at]timmy[dot]ws
--]]

-- Network strings!
if SERVER then
    util.AddNetworkString( "Toggle Eloquent GUI" )
end

-- Create ULX command
function ulx.eloquent( calling_ply )
    net.Start( "Toggle Eloquent GUI" )
    net.Send( calling_ply )
end
local eloquent = ulx.command( "Utility", "ulx eloquent", ulx.eloquent, { "!eloquent", "/eloquent" }, true )
eloquent:defaultAccess( ULib.ACCESS_ADMIN )
eloquent:help( "Toggle the Eloquent control panel." )
